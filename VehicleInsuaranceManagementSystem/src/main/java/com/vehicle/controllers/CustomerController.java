package com.vehicle.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import com.vehicle.models.Customer;
import com.vehicle.services.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService service;
	
	@GetMapping("/CustRegister")
	public String registerCustomer(@ModelAttribute("customer") Customer customer) {
		
		return "Registration";
	}
	
	@PostMapping("/addCust")
	public String addCustomer(@ModelAttribute("customer") Customer customer,Model model) {
		if(service.addCustomer(customer))
		{
			model.addAttribute("regMessage", "Registered Successfully");
		}
		else {
			model.addAttribute("regMessage", "Please Enter all the details correctly");
		}
		return "Registration";
	}
	@PostMapping("/custLogin")
	public String login(@RequestParam("userId") String userid,@RequestParam("password") String pass , Model model) {
		int r=service.findCustomer(userid, pass);
		if(r==1) {
			model.addAttribute("custName", userid);
			return "CustomerHome";
		}
		else if(r==0) { model.addAttribute("logMessage","User Id not present");
		return "home";
		}
		else {
			model.addAttribute("logMessage","Password not matching");
			return "home";
		}
	} 
}
