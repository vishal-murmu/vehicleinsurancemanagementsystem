package com.vehicle.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.vehicle.models.Customer;

@Component
public class CustomerDaoImpl implements CustomerDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public boolean addCustomer(Customer c) {
		// TODO Auto-generated method stub
		int r=jdbcTemplate.update("insert into customer (firstName,lastName,dob,gender,contactNumber,email,licenseNumber,userId,password) values(?,?,?,?,?,?,?,?,?)",c.getFirstName(),c.getLastName(),c.getDob(),c.getGender(),c.getContactNumber(),c.getEmail(),c.getLicenseNumber(),c.getUserId(),c.getPassword());
		if(r>0)
			return true;
		else return false;

	}


	@Override
	public Customer findCustomer(String userId) {
		// TODO Auto-generated method stub
		PreparedStatementSetter setter=new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, userId);
			}
		};
		
		
		return jdbcTemplate.query("select * from customer where userId=?",setter,new ResultSetExtractor<Customer>() {

			@Override
			public Customer extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				Customer c=null;
				if(rs.next()) {
					c=new Customer();
				c.setFirstName(rs.getString(1));
				c.setLastName(rs.getString(2));
				c.setDob(rs.getDate(3));
				c.setGender(rs.getString(4));
				c.setContactNumber(rs.getLong(5));
				c.setEmail(rs.getString(6));
				c.setLicenseNumber(rs.getString(7));
				c.setUserId(rs.getString(8));
				c.setPassword(rs.getString(9));
				
				}
				return c;
				}
		});
	}

}
