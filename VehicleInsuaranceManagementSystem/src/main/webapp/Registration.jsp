<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CustomerRegistration</title>
</head>
<body>
<form:form method="post" action="addCust" modelAttribute="customer">
<label>Enter First Name</label><br>
<form:input path="firstName" id="firstName"/><br>
<label>Enter Last Name</label><br>
<form:input path="lastName" id="lastName"/><br>
<label>Enter Date Of Birth</label><br>
<form:input path="dob" id="dob"/><br>
<label>Enter Gender</label><br>
<form:input path="gender" id="gender"/><br>
<label>Enter Contact Number</label><br>
<form:input path="contactNumber" id="ContactNumber"/><br>
<label>Enter Email</label><br>
<form:input path="email" id="email"/><br>
<label>Enter License Number</label><br>
<form:input path="licenseNumber" id="licenseNumber"/><br>
<label>Enter userId</label><br>
<form:input path="userId" id="userId"/><br>
<label>Enter Password</label><br>
<form:input path="password" id="password" type="password"/><br>
<input type="submit" value="Register"/><br>
</form:form>
<br>
<h2>${regMessage }</h2>
</body>
</html>